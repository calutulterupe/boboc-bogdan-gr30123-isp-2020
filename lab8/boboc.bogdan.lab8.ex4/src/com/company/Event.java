package com.company;

import javafx.event.EventType;

abstract class Event {

    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    public Event(com.company.EventType none) {
    }

    EventType getType() {
        return type;
    }

}