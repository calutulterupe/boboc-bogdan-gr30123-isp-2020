package lab9;

public class Main {

    public static void main(String[] args) {
        TicTacToe game = new TicTacToe();
        game.placeMark(0,2);
        game.printBoard();
        if (game.checkForWin()) {

            System.out.println("We have a winner! Congrats!");

            System.exit(0);

        }
        else if (game.isBoardFull()) {

            System.out.println("Appears we have a draw!");

            System.exit(0);

        }

        game.changePlayer();


    }
}
