package Colocviu;

import Colocviu.categories.BookCategory;
import Colocviu.categories.ToyCategory;
import Colocviu.entities.Book;
import Colocviu.entities.Product;
import Colocviu.entities.Site;
import Colocviu.entities.Toy;

import java.io.*;
import java.util.Scanner;

public class Main {

    static int lastbook=0;
    static int lasttoy=0;
    public static void main(String[] args) {
        boolean go=true;
        Site site=new Site();
        BookCategory carti=site.getCarti();
        ToyCategory jucarii=site.getJucarii();

        while(go)
        {
            System.out.println("Apasati 0 pentru a inchide");
            System.out.println("Apasati 1 pentru a adauga un produs:");
            System.out.println("Apasati 2 pentru a sterge un produs:");
            System.out.println("Apasati 3 pentru a edita un produs:");
            System.out.println("Apasati 4 pentru a pune un produs la vanzare:");
            System.out.println("Apasati 5 pentru a retrage un produs de la vanzare:");
            Scanner s = new Scanner(System.in);
            String str = s.nextLine();
            readfromfile(carti,jucarii);
            int val=0;
            switch(str)
            {
                case "1":
                    System.out.println("Apasati 1 pentru a adauga o jucarie");
                    System.out.println("Apasati altceva pentru a adauga o carte");
                    val = s.nextInt();
                    if(val==1)
                    {
                        jucarii.addProduct(addProduct(false));
                    }
                    else
                    {
                        carti.addProduct(addProduct(true));
                    }
                    break;
                case "2":
                    System.out.println("Apasati 1 pentru a sterge o jucarie");
                    System.out.println("Apasati altceva pentru a sterge o carte");
                    val = s.nextInt();
                    if(val==1)
                    {
                        jucarii.removeProduct(addProduct(false));
                    }
                    else
                    {
                        carti.removeProduct(addProduct(true));
                    }
                    break;
                case "3":
                    break;
                case "4":
                    System.out.println("Apasati 1 pentru a pune o jucarie la vanzare");
                    System.out.println("Apasati altceva pentru a pune o carte la vanzare");
                     val = s.nextInt();
                    if(val==1)
                    {
                        System.out.println("Care este id-ul jucarii?");
                        int id=s.nextInt();
                        jucarii.getProduct(id).markForSale();

                    }
                    else
                    {  System.out.println("Care este id-ul cartii?");
                        int id=s.nextInt();
                        jucarii.getProduct(id).markForSale();

                    }
                    break;
                case "5":
                    System.out.println("Apasati 1 pentru a retrtage o jucarie la vanzare");
                    System.out.println("Apasati altceva pentru a retrage o carte la vanzare");
                    val = s.nextInt();
                    if(val==1)
                    {
                        System.out.println("Care este id-ul jucarii?");
                        int id=s.nextInt();
                        jucarii.getProduct(id).retireFromSale();

                    }
                    else
                    {  System.out.println("Care este id-ul cartii?");
                        int id=s.nextInt();
                        jucarii.getProduct(id).retireFromSale();

                    }
                    break;
                case"0":
                    go=false;
                    break;
                default:
                    System.out.println("Ati introdus ceva gresit");
                    break;
            }
            try{
                write(carti,jucarii);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
    private static Product addProduct(boolean e_carte)
    {

        int id;
        if(e_carte)
            id=++lastbook;
        else
            id=++lasttoy;

        int price;
        String name;
        System.out.println("Apasati 1 pentru a  o face foSale");
        System.out.println("Apasati altceva pentru a nu o face forSale");
        Scanner s = new Scanner(System.in);
        String str = s.nextLine();

        System.out.println("introduceti numele");
        name = s.nextLine();
        System.out.println("introduceti pretul");
        price=s.nextInt();
        Product carte=new Product(id,name,price);
        if(str.equals("1"))
            carte.markForSale();
        else
            carte.retireFromSale();
        return carte;

    }
    public static void write( BookCategory carti, ToyCategory jucarii) throws IOException {
        File fout = new File("colocviu.txt");
        FileOutputStream fos = new FileOutputStream(fout);

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

        for (int i = 0; i < lastbook; i++) {
            bw.write(carti.getProduct(i).toString());
            bw.newLine();
        }
        for (int i = 0; i < lasttoy; i++) {
            bw.write(jucarii.getProduct(i).toString());
            bw.newLine();
        }
        bw.close();
    }
    public static void readfromfile(BookCategory carti,ToyCategory jucarii)
    {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader("colcoviu.txt"));
            String line = reader.readLine();
            while (line != null) {
                System.out.println(line);
                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
