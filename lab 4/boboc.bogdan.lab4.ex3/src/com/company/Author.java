package com.company;

public class Author {
    private String name;
    private String email;
    private char gender;

    public char getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public String getemail() {
        return email;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public void setemail(String email) {
        this.email = email;
    }

    public void setName(String name) {
        this.name = name;
    }
}
