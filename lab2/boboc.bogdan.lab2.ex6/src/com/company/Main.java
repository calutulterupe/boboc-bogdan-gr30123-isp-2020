package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	System.out.println("please insert N:");
        Scanner keyboard=new Scanner(System.in);
        int N=keyboard.nextInt();
        System.out.println(factorial_nerecursiv(N));
        System.out.println(factorial_recursiv(N));
    }
    private static int factorial_nerecursiv(int N)
    {
        int val=1;
        for(int i=1;i<=N;i++)
            val=val*i;
        return val;
    }
    private static int factorial_recursiv(int N)
    {
        if(N>1)
            N=N*factorial_recursiv(N-1);

        return N;
    }
}
