package com.company;

public class ProxyImage implements Image {
    private RealImage realImage;
    private String fileName;
    private RotationImage rotationImage;
    private boolean type;
    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    public ProxyImage(boolean type,String fileName){
        this.type=type;
        this.fileName = fileName;
    }
    @Override
    public void display() {
        if(realImage == null){
            if(type)
            realImage = new RealImage(fileName);
            else
                rotationImage=new RotationImage(fileName);
        }
        if(type)
            realImage.display();
        else
            rotationImage.display();
    }
}
