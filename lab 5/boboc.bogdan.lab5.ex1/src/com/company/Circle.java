package com.company;

import java.util.zip.CheckedInputStream;

public class Circle extends Shape {
    private double radius;
    public Circle(){};
    public Circle(double radius)
    {
        this.radius=radius;
    }
    public Circle(double radius,String color,boolean filled)
    {
        this.radius=radius;
        this.color=color;
        this.filled=filled;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getAreea() {
        return Math.PI*this.radius*this.radius;
    }

    @Override
    public double getParimeter() {
        return 2*Math.PI*this.radius;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius=" + radius +
                '}';
    }
}
