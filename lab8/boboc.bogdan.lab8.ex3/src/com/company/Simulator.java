package com.company;

public class Simulator {

    /**
     * @param args
     */
    public static void main(String[] args) {

        //build station Cluj-Napoca
        Controler c1 = new Controler("Cluj-Napoca");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);

        //build station Bucuresti
        Controler c2 = new Controler("Bucuresti");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);

        //connect the 2 controllers

        c2.setNeighbourController(c1);

        //testing

        Train t1 = new Train("Bucuresti", "IC-001");
        s1.arriveTrain(t1);

        Train t2 = new Train("Cluj-Napoca","R-002");
        s5.arriveTrain(t2);

        Train t3 = new Train("Sibiu","R-002");
        s4.arriveTrain(t3);

        c1.displayStationState();
        c2.displayStationState();

        System.out.println("\nStart train control\n");

        //build station Sibiu
        Controler c3 = new Controler("Sibiu");

        Segment s10 = new Segment(10);
        Segment s11 = new Segment(9);

        c1.addControlledSegment(s10);
        c1.addControlledSegment(s11);

        Train t11 = new Train("Sibiu","R-002");
        s11.arriveTrain(t11);

        Train t10 = new Train("Sibiu","R-002");
        s10.arriveTrain(t10);


        c2.setNeighbourController(c3);
        c3.setNeighbourController(c1);



        //execute 3 times controller steps
        for(int i = 0;i<2;i++){
            System.out.println("### Step "+i+" ###");
            c1.controlStep();
            c2.controlStep();
            c3.controlStep();

            System.out.println();

            c1.displayStationState();
            c2.displayStationState();
            c3.displayStationState();
        }
    }
}
