package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Encrpition_Class {
    private Encrpition_Class(){};
    static void Encrypt(String oldPath,String newPath)
    {
        try {
            File myObj = new File(oldPath);
            String encryptedfile="";
            Scanner myReader = new Scanner(myObj);

            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                for(int i=0;i<data.length();i++) {
                    encryptedfile+=(char)((int)data.charAt(i)+1);
                }
            }
            myReader.close();

            File encfile=new File(newPath);
            if(encfile.createNewFile())
            {
                FileWriter myWriter = new FileWriter(newPath);
                myWriter.write(encryptedfile);
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            }
            else
            {
                encfile.delete();
                encfile=new File(newPath);
                FileWriter myWriter = new FileWriter(newPath);
                myWriter.write(encryptedfile);
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            }

        } catch (FileNotFoundException e) {
            System.out.println("Couldn't find FIle");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static void Decrypt(String oldPath,String newPath)
    {
        try {
            File myObj = new File(oldPath);
            String encryptedfile="";
            Scanner myReader = new Scanner(myObj);

            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                for(int i=0;i<data.length();i++) {
                    encryptedfile+=(char)((int)data.charAt(i)-1);
                }
            }
            myReader.close();

            File encfile=new File(newPath);
            if(encfile.createNewFile())
            {
                FileWriter myWriter = new FileWriter(newPath);
                myWriter.write(encryptedfile);
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            }
            else
            {
                encfile.delete();
                encfile=new File(newPath);
                FileWriter myWriter = new FileWriter(newPath);
                myWriter.write(encryptedfile);
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            }

        } catch (FileNotFoundException e) {
            System.out.println("Couldn't find FIle");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
