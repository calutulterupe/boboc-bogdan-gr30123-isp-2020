package Colocviu;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class ProxyImageTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
    private String expected="IP's:\n" +
            "190.2.137.30\n" +
            "\n" +
            "Login (SSH or RDP):\n" +
            "Username : root or administrator\n" +
            "tsqDPRzYATadC8HZ\n" +
            "\n" +
            "https://www.google.com/recaptcha/api2/anchor?ar=1&k=6LfwuyUTAAAAAOAmoS0fdqijC2PbbdH4kjq62Y1b&co=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbTo0NDM.&hl=en&v=2diXFiiA9NsPIBTU15LG6xPf&size=normal&s=2jATf0_8hSsBVC_rATLkZR_bAa5UN4EXzOCoGbL_55Grk1zfPwCvojx-X-laNw30mmWsAQJNUClWDekPqYR-qac7tmBExFFTJ8zyCK4hurW0CxWOgsYl1wRHpgLzOlXlW7NurNyJ5n6IC-bXR2x0rTCNEZLOahxNWaECBvPOBCKbwSbzMXTpdPctU7xl8moWcS1zjxzAB-86PTd-ZrX0vF46hxJyE_b_ZJ0qcRvlvqA3b-7CjnS50PU&cb=mf6tx9y90yk3\n" +
            "\n" +
            "\n" +
            "\n";
    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    @Test
    void display() {
        ProxyImage proxy=new ProxyImage("C:\\Users\\Bogdan\\Deskto\\FrenchSSH credential.txt");
        proxy.display();
        assertEquals(expected,outContent.toString(),"Nu merge BOSS");
    }
}
