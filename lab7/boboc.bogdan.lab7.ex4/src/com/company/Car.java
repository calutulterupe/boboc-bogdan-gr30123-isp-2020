package com.company;

import java.io.*;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Car implements Serializable {
    private String model;
    private Double price;
    public Car(String model,Double price)
    {
        this.model=model;
        this.price=price;
    }
    public void saveCar(String path)
    {
        try {
            FileOutputStream f = new FileOutputStream(new File(path));
            ObjectOutputStream o = new ObjectOutputStream(f);
            // Write objects to file
            o.writeObject(this);
            o.close();
            f.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream:"+e.getMessage());
        }
    }
    static public void read(String path)
    {
        try {
            FileInputStream fi = new FileInputStream(new File(path));
            ObjectInputStream oi = new ObjectInputStream(fi);
            // Read objects
            Car pr1 = (Car) oi.readObject();
            System.out.println(pr1.toString());
            oi.close();
            fi.close();
        }
        catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", price=" + price +
                '}';
    }


}
