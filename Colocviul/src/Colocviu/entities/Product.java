package Colocviu.entities;

import Colocviu.actions.RetireProduct;
import Colocviu.actions.SellProduct;

public class Product extends ProductDetails implements SellProduct, RetireProduct {
    protected boolean forSale;
    protected int categoryId;
    public Product(){}
    public Product(int id,String name,int price) {
        this.id=id;
        this.name=name;
        this.price=price;
    }
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void markForSale(){
        forSale=true;

    }
    public void retireFromSale()
    {
        forSale=false;
    }
    @Override
    public String toString() {
        return this.name+"+"+this.id+"+"+this.categoryId+"+"+this.price+"+"+this.forSale;
    }
}
