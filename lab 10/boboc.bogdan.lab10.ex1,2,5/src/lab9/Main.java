package lab9;

public class Main {

    public static void main(String[] args) {
        JoinTest w1 = new JoinTest("Proces 1",null);
        JoinTest w2 = new JoinTest("Proces 2",w1);
        w1.start();
        w2.start();
    }
}
