package com.company;

public class BankAccount {
    private String owner;
    private double balance;
    public BankAccount(String owner,double balanta)
    {
        this.owner=owner;
        this.balance=balanta;
    }
    public BankAccount()
    {

    }
    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
    public void withdraw(double value)
    {
        balance=balance-value;
    }
    public void deposit(double value)
    {
        balance=balance+value;
    }
    @Override
    public boolean equals(final Object obj) {
        BankAccount account=(BankAccount)obj;
        if(account==null)
            return false;
        if(account.hashCode()==this.hashCode())
            return true;
        return false;
    }
    @Override
    public int hashCode() {
        //e un cod in functie de valorile salvate in obiect
        int result = 15*owner.hashCode();
        result=result+(int)(balance*100);
        return result;
    }

}
