package lab9;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        JTextField field=new JTextField("pleace put the path here");
        field.setBounds(30,30,100,100);
        JButton btn=new JButton();
        btn.setBounds(30,110,100,40);
        JTextField txt=new JTextField();
        txt.setBounds(30,300,200,200);
        SimpleApp a=new SimpleApp();
        a.add(btn);
        a.add(field);
        a.add(txt);
        btn.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                // display/center the jdialog when the button is pressed
                Path path = Paths.get(field.getText());
                try {
                    byte[] bytes = Files.readAllBytes(path);
                    txt.setText(Files.readAllLines(path, StandardCharsets.UTF_8).toString());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
