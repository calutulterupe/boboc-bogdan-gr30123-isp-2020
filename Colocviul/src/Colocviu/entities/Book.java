package Colocviu.entities;

import Colocviu.entities.Product;

public class Book extends Product {
    public Book(Product produs,int category) {
        this.categoryId=category;
        this.forSale=produs.forSale;
        this.id=produs.id;
        this.name=produs.name;
        this.price=produs.price;
    }
    public Book(int id,String name,int price) {

        this.id=id;
        this.name=name;
        this.price=price;
    }

    @Override
    public void markForSale(){
        super.markForSale();
    }

    @Override
    public void retireFromSale()
    {
        super.retireFromSale();
    }


}
