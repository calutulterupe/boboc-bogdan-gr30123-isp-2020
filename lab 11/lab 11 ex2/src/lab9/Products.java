package lab9;

import java.awt.geom.QuadCurve2D;

public class Products {
    private String Name=new String();
    private int Quantity=0;
    private int Price=0;
    @Override
    public String toString()
    {
        return "Produs:"+Name+" Quantity:"+Quantity+" Price:"+Price;
    }
    public Products(String name,int quantity,int price)
    {
        this.Name=name;
        this.Quantity=quantity;
        this.Price=price;
    }
    public void setQuantity(int new_quantity)
    {
        this.Quantity=new_quantity;
    }

    public boolean equals(Products target)
    {
        if(target.toString()==this.toString())
            return  true;
        return false;
    }

}
