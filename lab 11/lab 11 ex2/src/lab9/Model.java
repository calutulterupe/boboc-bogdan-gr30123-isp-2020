package lab9;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private ArrayList<Products> Lista;
    public Model()
    {
        Lista=new ArrayList<>();
    }
    public void add_Product(Products produs)
    {
        Lista.add(produs);
    }
    public ArrayList<Products> getList()
    {
        return Lista;
    }
    public void delete_Product(Products produs)
    {
        for(int i=0;i<Lista.size();i++)
        {
            if(Lista.get(i).equals(produs))
            {   Lista.remove(i);
                return;
            }
        }

    }
    public void change_quantity(Products produs,int quantity)
    {
        for(int i=0;i<Lista.size();i++)
        {
            if(Lista.get(i).equals(produs))
            {   Lista.get(i).setQuantity(quantity);
                return;
            }
        }
    }
}
