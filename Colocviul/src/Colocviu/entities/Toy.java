package Colocviu.entities;

import Colocviu.entities.Product;

public class Toy extends Product {
    public Toy(Product produs,int category) {
        this.categoryId=category;
        this.forSale=produs.forSale;
        this.id=produs.id;
        this.name=produs.name;
        this.price=produs.price;
    }

    @Override
    public void markForSale(){
        super.markForSale();
    }

    @Override
    public void retireFromSale()
    {
        super.retireFromSale();
    }

}
