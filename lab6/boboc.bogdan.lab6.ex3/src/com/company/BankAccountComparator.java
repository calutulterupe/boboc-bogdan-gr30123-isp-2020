package com.company;

import java.util.Comparator;

public class BankAccountComparator implements Comparator<BankAccount> {

        @Override
        public int compare(BankAccount e1, BankAccount  e2) {
            return e1.getOwner().compareTo(e2.getOwner());
        }

}
