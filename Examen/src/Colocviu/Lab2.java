package Colocviu;

import java.util.ArrayList;
import java.util.Random;

public class Lab2 {
    public ArrayList<Integer> orderList(ArrayList<Integer> list)
    {
        Integer temp=0;

        if (list.size()>1) // check if the number of orders is larger than 1
        {
            for (int x=0; x<list.size(); x++) // bubble sort outer loop
            {
                for (int i=0; i < list.size()-x-1; i++) {
                    if (list.get(i)>list.get(i+1))
                    {
                        temp = list.get(i);
                        list.set(i,list.get(i+1) );
                        list.set(i+1, temp);
                    }
                }
            }
        }
        return list;
    }
    public ArrayList<Integer> generateRandomlist(int n)
    {
        Random random=new Random();
        ArrayList<Integer> lista=new ArrayList<Integer>();
        for(int i=0;i<n;i++)
        {

            Integer valoare = random.nextInt(100);
            lista.add(valoare);

        }
        return lista;
    }
    public int factorialNonRecursiv(int N)
    {
        int val=1;
        for(int i=1;i<=N;i++)
        {
            val=val*i;
        }
        return val;
    }
    public int factorialRecursiv(int N)
    {
        if(N==0)
            return 1;
        else
            return N*factorialRecursiv(N-1);

    }
}
