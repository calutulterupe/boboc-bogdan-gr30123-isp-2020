package Colocviu.categories;

import Colocviu.categories.Category;
import Colocviu.entities.Book;
import Colocviu.entities.Product;

import java.util.ArrayList;
import java.util.List;

public class BookCategory extends Category {
    ArrayList<Book> lista=new ArrayList<>();
    public BookCategory()
    {
        this.id=1;
    }
    @Override
    public void addProduct(Product p){
        Book carte=new Book(p,this.id);
        lista.add(carte);
    }
    @Override
    public void removeProduct(Product p){
        Book carte=new Book(p,this.id);
        if(lista.indexOf(carte)>-1)
            lista.remove(lista.indexOf(carte));

    }
    @Override
    public void updateProduct(Product p){
        Book carte=new Book(p,this.id);
        if(lista.indexOf(carte)>-1) {
            int i=lista.indexOf(carte);

        }

    }
    @Override
    public Product getProduct(int id){
        return lista.get(id);

    }

}
