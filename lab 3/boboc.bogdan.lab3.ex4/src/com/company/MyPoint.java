package com.company;
/*
Two instance variables x (int) and y (int).
A “no-argument” (or “no-arg”) constructor that construct a point at (0, 0).
A constructor that constructs a point with the given x and y coordinates.
Getter and setter for the instance variables x and y.
A method setXY() to set both x and y.
A toString() method that returns a string description of the instance in the format “(x, y)”.
A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates.
An overloaded distance(MyPoint another) that returns the distance from this point to the given MyPoint instance another.
 */
public class MyPoint {
    private  int x;
    private  int y;
    public MyPoint(){
        x=0;
        y=0;

    }
    public MyPoint(int x,int y)
    {
        this.x=x;
        this.y=y;

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
    public  void SetXY(int x,int y)
    {
        this.x=x;
        this.y=y;
    }
    public String ToString()
    {
        return "("+String.valueOf(x)+","+String.valueOf(y)+")";

    }
    public double distance(int x, int y)
    {
        Double myx= (double) this.x;
        Double myy= (double) this.y;
        Double new_x= (double) x;
        Double new_y= (double) y;

        return Math.sqrt((myx*myx-new_x*new_x)+(myy*myy-new_y*new_y));
    }
    public double distance(MyPoint another)
    {
        Double myx= (double) this.x;
        Double myy= (double) this.y;
        Double new_x= (double) another.getX();
        Double new_y= (double) another.getY();

        return Math.sqrt((myx-new_x)*(myx-new_x)+(myy-new_y)*(myy-new_y));
    }
}
