package com.company;

public class Circle {
    protected double radius=1;
    protected String color="red";

    public Circle(){}

    public Circle(double radius)
    {
        this.radius=radius;
    }

    public double getRadius() {
        return radius;
    }

    public String getColor() {
        return color;
    }



    @Override
    public String toString()
    {
        return "Cerc cu aria"+radius+"culoarea red";
    }
}
