package Colocviu;

public class Robot{
    int x;
    public Robot()
    {
        x=1;
    }
    public void change(int k) throws InvalidPositionException {
        if(k>=1)
            x=k;
        else
            throw new InvalidPositionException();
    }

    @Override
    public String toString() {
        return "Robot{" +
                "x=" + x +
                '}';
    }

}

