package lab9;

public class Controller {
    private Model model;
    private View view;
    public Controller(Model modelul,View vedere)
    {
        view=vedere;
        model=modelul;
    }
    public void add_new_product(Products produs)
    {
        model.add_Product(produs);
        view.add_new_product(produs);
    }
    public void delete_product(Products produs)
    {
        model.delete_Product(produs);
        view.delete_product(produs);
    }
    public void change_quantity(Products produs,int quantity)
    {
        model.change_quantity(produs,quantity);
        view.change_quantity(produs,quantity);
    }
    public void viewall()
    {

        view.ViewAll();
    }
}
