package lab9;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {

    public static void main(String[] args) {
        SimpleApp a=new SimpleApp();

        JButton btn=new JButton();
        btn.setBounds(5,5,30,30);
        JLabel label=new JLabel("0");
        label.setBounds(40,40,40,40);
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                label.setText(Integer.toString(Integer.parseInt(label.getText())+1));
            }
        });
        a.add(btn);
        a.add(label);
    }
}
