package lab9;

public class Main {

    public static void main(String[] args) {
	Model model=new Model();
	Products produs1=new Products("cai",10,1000);
        Products produs2=new Products("vaci",89,33);
        Products produs3=new Products("iepuri",165,16400);
        Products produs4=new Products("otel",100,500);
        View view=new View(model);
        Controller control=new Controller(model,view);
        control.add_new_product(produs1);
        control.add_new_product(produs2);
        control.add_new_product(produs3);
        control.add_new_product(produs4);
        control.viewall();
        control.delete_product(produs1);
        control.viewall();
        control.change_quantity(produs2,3);
        control.viewall();

    }
}
