package com.company;

public class Circle {
    private double Radius=1.0;
    private String Color="Red";
    public Circle(double newradius)
    {
        this.Radius=newradius;
    }
    public Circle(double newradius,String color)
    {
        this.Radius=newradius;
    }
    public double getRadius()
    {
        return Radius;

    }
    public double getArea()
    {
        return Radius*Radius*Math.PI;
    }


}
