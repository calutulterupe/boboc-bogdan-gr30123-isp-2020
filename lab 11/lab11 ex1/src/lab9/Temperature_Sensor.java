package lab9;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Temperature_Sensor extends Thread {
    float temp=0;
    @Override
    public void run()
    {

        float new_temp= ThreadLocalRandom.current().nextFloat()*30;
        if(temp!=new_temp) {
            System.out.println("Temperature has CHanged..");
            temp=new_temp;
        }
        System.out.println(temp);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.run();
    }
}
