package com.company;
/*
Three private instance variables: name (String), email (String), and gender (char of either 'm' or 'f');
One constructor to initialize the name, email and gender with the given values;
public Author (String name, String email, char gender) {……}
(There is no default constructor for Author, as there are no defaults for name, email and gender.)
public getters/setters: getName(), getEmail(), setEmail(), and getGender();
(There are no setters for name and gender, as these attributes cannot be changed.)
A toString() method that returns “author-name (gender) at email”, e.g., “My Name (m) at myemail@somewhere.com”.
 */
public class Author {
    private String name;
    private String email;
    private char gender;
    public Author(String new_name,String new_email,char new_gender)
    {
        name=new_name;
        email=new_email;
        gender=new_gender;
    }

    public String getName() {
        return name;
    }

    public char getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }
    public void setName(String name)
    {
        this.name=name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }
    public String ToString()
    {
        return name+" "+gender+" at "+email;
    }
}
