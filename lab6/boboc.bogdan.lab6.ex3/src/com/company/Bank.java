package com.company;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;


public class Bank {


    private TreeSet<BankAccount> banks=new TreeSet<BankAccount>(new BankAccIntComp());
    public void AddAccount(String owner,double value)
    {
        BankAccount account=new BankAccount(owner,value);
        banks.add(account);

    }
    public void printAccounts(int min,int max)
    {

        if(max>banks.size())
        {
            for(BankAccount e:banks){
                System.out.println("Bank Account owner:"+e.getOwner()+" with balance="+e.getBalance());
            }

        }
        else
        {
            for(BankAccount e:banks){
                System.out.println("Bank Account owner:"+e.getOwner()+" with balance="+e.getBalance());
            }
        }
    }

    public void printAccounts()
    {
        for(BankAccount e:banks){
            System.out.println("Bank Account owner:"+e.getOwner()+" with balance="+e.getBalance());
        }
    }
    public void  getAllAccounts()
    {
         TreeSet<BankAccount> banks_string=new TreeSet<BankAccount>(new BankAccountComparator());
        banks_string.addAll(banks);
        for(BankAccount e:banks_string){
            System.out.println("Bank Account owner:"+e.getOwner()+" with balance="+e.getBalance());
        }
    }




}
