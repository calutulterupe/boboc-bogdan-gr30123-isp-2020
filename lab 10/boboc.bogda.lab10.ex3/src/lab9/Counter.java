package lab9;

public class Counter extends Thread {
    boolean done=false;

    Counter(String name){
        super(name);
    }

    public void run(boolean mode) throws InterruptedException {
        if(mode) {
            for (int i = 0; i < 100; i++) {
                System.out.println(getName() + " i = " + i);
                try {
                    Thread.sleep((int) (Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(getName() + " job finalised.");
            done = true;
        }
        else {
            while(!this.done)
            {
                Thread.sleep(100);

            }
            for(int i=100;i<200;i++){
                System.out.println(getName() + " i = "+i);
                try {
                    Thread.sleep((int)(Math.random() * 1000));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println(getName() + " job finalised.");
        }


    }


}