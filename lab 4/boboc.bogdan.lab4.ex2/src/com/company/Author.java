package com.company;

public class Author {
    private String name;
    private String email;
    private char gender;
    public Author(String new_name,String new_email,char new_gender)
    {
        name=new_name;
        email=new_email;
        gender=new_gender;
    }

    public String getName() {
        return name;
    }

    public char getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }
    public void setName(String name)
    {
        this.name=name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }
    public String ToString()
    {
        return name+" "+gender+" at "+email;
    }
}
