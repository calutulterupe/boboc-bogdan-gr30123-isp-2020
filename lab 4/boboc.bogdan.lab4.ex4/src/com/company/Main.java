package com.company;

public class Main {

    public static void main(String[] args) {
        Author[] authors=new Author[3];
        authors[0]=new Author();
        authors[1]=new Author();
        authors[2]=new Author();
        authors[0].setName("Vasile Mincinosu");
        authors[1].setName("Andrei Bombardieru");
        authors[2].setName("Andrea Flagrant");
        authors[0].setGender('m');
        authors[1].setGender('m');
        authors[2].setGender('f');
        authors[0].setemail("nimeni@yahoo.com");
        authors[1].setemail("toti@gmail.com");
        authors[2].setemail("nope@gmail.com");
        Book book=new Book("Cum sa castigi",authors,321,22);
        System.out.println(book.toString());

    }
}
