package com.company;

public class Cilinder extends Circle {
    private double height=1;

    public Cilinder()
    {

    }
    public Cilinder(double radius)
    {
        this.radius=radius;
    }
    public Cilinder(double radius,double height)
    {
        this.radius=radius;
        this.height=height;
    }
    public double getHeight() {
        return height;
    }
    public double getVolume()
    {
        return this.height*2*Math.PI*this.radius*this.radius;
    }
    public void setHeight(double height) {
        this.height = height;
    }
}
