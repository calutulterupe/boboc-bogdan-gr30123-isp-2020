package Colocviu.categories;

import Colocviu.entities.Book;
import Colocviu.entities.Product;
import Colocviu.entities.Toy;
import java.util.ArrayList;

public class ToyCategory extends Category {
    ArrayList<Toy> lista=new ArrayList<Toy>();
    public ToyCategory()
    {
        this.id=2;
    }
    @Override
    public void addProduct(Product p){
        Toy jucarie=new Toy(p,this.id);
        lista.add(jucarie);
    }
    @Override
    public void removeProduct(Product p){
        Toy jucarie=new Toy(p,this.id);
        if(lista.indexOf(jucarie)>-1)
            lista.remove(lista.indexOf(jucarie));

    }
    @Override
    public void updateProduct(Product p){
        Toy jucarie=new Toy(p,this.id);
        if(lista.indexOf(jucarie)>-1) {
            int i=lista.indexOf(jucarie);

        }

    }
    @Override
    public Product getProduct(int id){
        return lista.get(id);

    }
}
