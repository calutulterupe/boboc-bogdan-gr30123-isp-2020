package com.company;

public class Controller implements TemperatureSensor,LightSensor {
    static private  Controller instance=null;
    private Controller(){};
    static public Controller CreateController()
    {
       instance=new Controller();
       return instance;
    }
    @Override
    public int getTempValue() {
        return temprandom.nextInt();
    }
    private  boolean go=true;
    @Override
    public int getLighValue() {
        return lightrandom.nextInt();
    }
    public void control() throws InterruptedException {
        while (go) {
            int temp = this.getTempValue();
            int light = this.getLighValue();
            System.out.println("Temperatur: "+temp+" Light:"+light);
            Thread.sleep(20000);
        }
    }

    public boolean isGo() {
        return go;
    }

    public void setGo(boolean go) {
        System.out.println("setgo");
        this.go = go;
    }
}
