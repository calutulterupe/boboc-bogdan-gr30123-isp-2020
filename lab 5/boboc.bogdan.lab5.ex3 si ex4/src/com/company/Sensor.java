package com.company;

public abstract class Sensor implements TemperatureSensor,LightSensor {
    private String location;

    public String getLocation() {
        return location;
    }
    abstract int readValue();

    @Override
    public int getLighValue() {
        return lightrandom.nextInt();
    }

    @Override
    public int getTempValue() {
        return temprandom.nextInt();
    }
}
