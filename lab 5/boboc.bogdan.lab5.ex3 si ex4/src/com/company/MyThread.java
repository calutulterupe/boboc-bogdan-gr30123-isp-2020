package com.company;

public class MyThread extends Thread {
    public MyThread( Controller controller)
    {
        try {
            controller.control();
        }
        catch (Exception ex)
        {
            String s=ex.getMessage();
            System.out.println(s);
        }

    }
}
