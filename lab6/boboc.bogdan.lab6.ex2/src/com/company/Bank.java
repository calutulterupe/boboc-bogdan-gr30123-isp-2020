package com.company;

import java.util.ArrayList;
import java.util.Comparator;


public class Bank {
    private ArrayList<BankAccount> banks= new ArrayList<BankAccount>();
     public void AddAccount(String owner,double value)
    {
        BankAccount account=new BankAccount(owner,value);
        banks.add(account);
    }
    public void printAccounts(int min,int max)
    {
        banks.sort(Comparator.comparing(BankAccount::getBalance).thenComparing(BankAccount::getBalance));
        if(max>banks.size())
        {
            for(int i=min;i<banks.size();i++) {
                System.out.println("Bank Account owner:"+banks.get(i).getOwner()+" with balance="+banks.get(i).getBalance());
            }
        }
        else
        {
            for(int i=min;i<max;i++) {
                System.out.println("Bank Account owner:"+banks.get(i).getOwner()+" with balance="+banks.get(i).getBalance());
            }
        }
    }

    public void printAccounts()
    {
        banks.sort(Comparator.comparing(BankAccount::getBalance).thenComparing(BankAccount::getBalance));
        for(int i=0;i<banks.size();i++) {
        System.out.println("Bank Account owner:"+banks.get(i).getOwner()+" with balance="+banks.get(i).getBalance());
        }
    }
    public void  getAllAccounts()
    {
        banks.sort(Comparator.comparing(BankAccount::getOwner).thenComparing(BankAccount::getOwner));
        for(int i=0;i<banks.size();i++) {
            System.out.println("Bank Account owner:"+banks.get(i).getOwner()+" with balance="+banks.get(i).getBalance());
        }
    }



}
