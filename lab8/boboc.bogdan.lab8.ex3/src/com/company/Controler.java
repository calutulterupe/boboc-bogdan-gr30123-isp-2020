package com.company;

import java.util.*;

public class Controler {
    String stationName;

    List<Controler> neighbourController=new List<Controler>() {
        @Override
        public int size() {
            return 0;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }

        @Override
        public boolean contains(Object o) {
            return false;
        }

        @Override
        public Iterator<Controler> iterator() {
            return null;
        }

        @Override
        public Object[] toArray() {
            return new Object[0];
        }

        @Override
        public <T> T[] toArray(T[] a) {
            return null;
        }

        @Override
        public boolean add(Controler controler) {
            return false;
        }

        @Override
        public boolean remove(Object o) {
            return false;
        }

        @Override
        public boolean containsAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean addAll(Collection<? extends Controler> c) {
            return false;
        }

        @Override
        public boolean addAll(int index, Collection<? extends Controler> c) {
            return false;
        }

        @Override
        public boolean removeAll(Collection<?> c) {
            return false;
        }

        @Override
        public boolean retainAll(Collection<?> c) {
            return false;
        }

        @Override
        public void clear() {

        }

        @Override
        public Controler get(int index) {
            return null;
        }

        @Override
        public Controler set(int index, Controler element) {
            return null;
        }

        @Override
        public void add(int index, Controler element) {

        }

        @Override
        public Controler remove(int index) {
            return null;
        }

        @Override
        public int indexOf(Object o) {
            return 0;
        }

        @Override
        public int lastIndexOf(Object o) {
            return 0;
        }

        @Override
        public ListIterator<Controler> listIterator() {
            return null;
        }

        @Override
        public ListIterator<Controler> listIterator(int index) {
            return null;
        }

        @Override
        public List<Controler> subList(int fromIndex, int toIndex) {
            return null;
        }
    };

    //storing station train track segments
    ArrayList<Segment> list = new ArrayList<Segment>();

    public Controler(String gara) {
        stationName = gara;
    }

    void setNeighbourController(Controler v){
        neighbourController.add(v);
    }

    void addControlledSegment(Segment s){
        list.add(s);
    }

    /**
     * Check controlled segments and return the id of the first free segment or -1 in case there is no free segment in this station
     *
     * @return
     */
    int getFreeSegmentId(){
        for(Segment s:list){
            if(s.hasTrain()==false)
                return s.id;
        }
        return -1;
    }

    void controlStep(){
        //check which train must be sent
        for (int i=0;i<neighbourController.size();i++) {

        for(Segment segment:list){
            if(segment.hasTrain()){
                Train t = segment.getTrain();

                if(t.getDestination().equals(neighbourController.get(i).stationName)){
                    //check if there is a free segment
                    int id = neighbourController.get(i).getFreeSegmentId();
                    if(id==-1){
                        System.out.println("Trenul +"+t.name+"din gara "+stationName+" nu poate fi trimis catre "+neighbourController.get(i).stationName+". Nici un segment disponibil!");
                        return;
                    }
                    //send train
                    System.out.println("Trenul "+t.name+" pleaca din gara "+stationName +" spre gara "+neighbourController.get(i).stationName);
                    segment.departTRain();
                    neighbourController.get(i).arriveTrain(t,id);
                }

            }
            }
        }//.for

    }//.


    public void arriveTrain(Train t, int idSegment){
        for(Segment segment:list){
            //search id segment and add train on it
            if(segment.id == idSegment)
                if(segment.hasTrain()==true){
                    System.out.println("CRASH! Train "+t.name+" colided with "+segment.getTrain().name+" on segment "+segment.id+" in station "+stationName);
                    return;
                }else{
                    System.out.println("Train "+t.name+" arrived on segment "+segment.id+" in station "+stationName);
                    segment.arriveTrain(t);
                    return;
                }
        }

        //this should not happen
        System.out.println("Train "+t.name+" cannot be received "+stationName+". Check controller logic algorithm!");

    }


    public void displayStationState(){
        System.out.println("=== STATION "+stationName+" ===");
        for(Segment s:list){
            if(s.hasTrain())
                System.out.println("|----------ID="+s.id+"__Train="+s.getTrain().name+" to "+s.getTrain().destination+"__----------|");
            else
                System.out.println("|----------ID="+s.id+"__Train=______ catre ________----------|");
        }
    }
}
