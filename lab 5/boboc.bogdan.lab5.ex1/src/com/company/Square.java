package com.company;

import com.sun.org.apache.bcel.internal.generic.ARETURN;

import java.util.stream.Stream;

public class Square extends Rectangle {
    public Square(){};
    public Square(double size)
    {
        this.length=size;
        this.width=size;
    }
    public Square(double size, String Color,boolean filled)
    {
        this.width=size;
        this.length=size;
        this.color=Color;
        this.filled=filled;
    }
    public double getSide()
    {
        return this.length;
    }
    public void setSide(double side)
    {
        this.width=side;
        this.length=side;
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
