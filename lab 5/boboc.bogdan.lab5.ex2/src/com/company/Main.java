package com.company;

public class Main {

    public static void main(String[] args) {
        ProxyImage image1=new ProxyImage(true,"iepure");
        image1.display();
        ProxyImage image2=new ProxyImage(false,"cal");
        image2.display();
    }
}