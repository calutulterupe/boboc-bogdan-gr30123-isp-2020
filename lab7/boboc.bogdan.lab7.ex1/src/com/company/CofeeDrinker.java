package com.company;

public class CofeeDrinker {
    void drinkCofee(Cofee c) throws TemperatureException, ConventrationException {
        if (c.getTemp() > 60)
            throw new TemperatureException(c.getTemp(), "Cofee is to hot!");
        if (c.getConc() > 50)
            throw new ConventrationException(c.getConc(), "Cofee concentration to high!");
        System.out.println("Drink cofee:" + c);
    }
}
