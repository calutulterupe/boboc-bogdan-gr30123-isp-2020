package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for(int i = 0;i<15;i++){
            Cofee c;
            try {
                c= mk.makeCofee();
            }
            catch (NumberException e)
            {
                System.out.println("too many coffes");
                break;
            }
            try {
                d.drinkCofee(c);
            } catch (TemperatureException e) {
                System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
            } catch (ConventrationException e) {
                System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
            }
            finally{
                System.out.println("Throw the cofee cup.\n");
            }
        }
    }
}
