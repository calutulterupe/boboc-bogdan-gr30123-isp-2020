package Colocviu;

import org.junit.runner.JUnitCore;
import sun.java2d.pipe.SpanShapeRenderer;

import java.awt.event.TextListener;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] Args)
    {
        //new ProxyImageTest().display();
        SimpleApp app=new SimpleApp();
        new ButtonAndTextField();
    }

    public static void mainLab3(String[] args)
    {
        Scanner tastatura=new Scanner(System.in);
        Robot robot=new Robot();
        try {
            int k=tastatura.nextInt();
            robot.change(k);
        }
        catch (InvalidPositionException exception)
        {
            System.out.println(exception);
        }
        finally {
            System.out.print(robot.toString());
        }
    }

    public static void mainLab2(String[] args) {
//	// citire tastatura
//        Scanner tastatura=new Scanner(System.in);
//        int intrare=tastatura.nextInt();
//        System.out.println(intrare);
        Lab2 laboratorul2=new Lab2();
        ArrayList<Integer> lista=laboratorul2.generateRandomlist(10);
        System.out.println(lista.toString());
        lista=laboratorul2.orderList(lista);
        System.out.println(lista);
        int factor1=laboratorul2.factorialNonRecursiv(5);
        int factor2=laboratorul2.factorialRecursiv(5);
        System.out.println(factor1+"=="+factor2);

    }

}
