package com.company;

import java.util.Comparator;

public class BankAccIntComp implements Comparator<BankAccount> {

    @Override
    public int compare(BankAccount e1, BankAccount  e2) {

        return (int) (e1.getBalance()-e2.getBalance());
    }
}
