package Colocviu.entities;

import Colocviu.categories.BookCategory;
import Colocviu.categories.ToyCategory;

public class Site {
    private String name;
    private String url;
    private BookCategory carti;
    private ToyCategory jucarii;
    public Site()
    {
        this.initCategories();
    }
    public Site(String name,String url){}
    public void initCategories(){
        carti=new BookCategory();
        jucarii=new ToyCategory();
    }

    public BookCategory getCarti() {
        return carti;
    }

    public ToyCategory getJucarii() {
        return jucarii;
    }
}
